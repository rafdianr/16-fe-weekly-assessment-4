# 16-FE-weekly-assessment-4

**Deadline**: 30 December 2021

## Task

- Go to this link, https://codesandbox.io/s/assessment-4-qu9kt
- Complete the task
- Don't forget to save
- Send the saved link to the discord channel
